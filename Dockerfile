FROM ubuntu:latest
COPY target/release/liturgia /
ENV ROCKET_ADDRESS=0.0.0.0

CMD /liturgia
EXPOSE 8000
