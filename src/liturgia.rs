use eprex_parser::types::DayPart;
use once_cell::sync::Lazy;
use std::collections::HashMap;

static EPREX_DATA: Lazy<(
    HashMap<u64, Vec<(u32, eprex_parser::types::DayFlags)>>,
    HashMap<u32, String>,
)> = Lazy::new(|| {
    let bytes = include_bytes!("../eprex.dat");
    let bytes = inflate::inflate_bytes(bytes).unwrap();
    let mut record_iter: eprex_parser::decoding::RecordIterator<_> = bytes.into_iter().into();
    record_iter.fold_to_dicts()
});

fn is_to_be_rendered(
    flags: &eprex_parser::types::DayFlags,
    keep: &eprex_parser::types::DayPart,
) -> bool {
    (match keep {
        DayPart::Ufficio => flags.ufficio,
        DayPart::Lodi => flags.lodi,
        DayPart::Terza => flags.terza,
        DayPart::Sesta => flags.sesta,
        DayPart::Nona => flags.nona,
        DayPart::Vespri => flags.vespri,
        DayPart::Compieta => flags.compieta,
        DayPart::Liturgia => flags.liturgia,
        DayPart::Santi => flags.santi,
        DayPart::Defunti => flags.defunti,
    }) && (flags.ufficio
        || flags.lodi
        || flags.terza
        || flags.sesta
        || flags.nona
        || flags.vespri
        || flags.compieta
        || flags.liturgia
        || flags.santi
        || flags.defunti)
}

pub fn get_for_day(day: u64, kind: Option<&eprex_parser::types::DayPart>) -> Vec<Vec<String>> {
    EPREX_DATA
        .0
        .get(&day)
        .map(|x| x.to_vec())
        .unwrap_or_default()
        .into_iter()
        .filter(|(_, df)| -> bool {
            match &kind {
                None => true,
                Some(kind) => {
                    if df.nan {
                        true
                    } else {
                        match kind {
                            DayPart::Ufficio | DayPart::Lodi => {
                                (df.biennale && !df.santi && !df.defunti)
                                    && (is_to_be_rendered(&df, kind))
                            }
                            DayPart::Terza
                            | DayPart::Sesta
                            | DayPart::Nona
                            | DayPart::Vespri
                            | DayPart::Compieta
                            | DayPart::Liturgia => {
                                (!(df.lodi && df.ufficio)
                                    && !df.biennale
                                    && !df.santi
                                    && !df.defunti)
                                    && (is_to_be_rendered(&df, kind.clone()))
                            }
                            DayPart::Santi => {
                                !df.lodi
                                    && !df.ufficio
                                    && !df.biennale
                                    && !df.defunti
                                    && is_to_be_rendered(&df, kind.clone())
                            }
                            DayPart::Defunti => {
                                !df.lodi
                                    && !df.ufficio
                                    && !df.biennale
                                    && !df.santi
                                    && is_to_be_rendered(&df, kind.clone())
                            }
                        }
                    }
                }
            }
        })
        .map(|(t_id, df)| (EPREX_DATA.1.get(&t_id).unwrap().clone(), df))
        .map(|(t, _)| html2text::from_read(t.as_bytes(), 1e9 as usize))
        .map(|x| x.trim().replace("\n\n", "\r\n\n"))
        .filter(|x| x.len() > 0)
        .map(|x| {
            x.split("\n")
                .filter(|x| x.len() > 0)
                .map(String::from)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
}
