#[macro_use]
extern crate rocket;
use parsers::Parse;
use rocket::serde::json::Json;

mod liturgia;
mod parsers;

fn get_today() -> u64 {
    let time = std::time::SystemTime::now();
    fix_date(
        time.duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_secs(),
    )
}

fn fix_date(date: u64) -> u64 {
    let time = std::time::UNIX_EPOCH + std::time::Duration::from_secs(date);
    let oadate = eprex_parser::compat::OLEAutomationDateCompatibility::to_oadate(&time);
    let converted: std::time::SystemTime =
        eprex_parser::compat::OLEAutomationDateCompatibility::from(oadate as u64 as f64);
    converted
        .duration_since(std::time::UNIX_EPOCH)
        .unwrap()
        .as_secs()
}

#[get("/")]
fn today_all() -> Json<Vec<Vec<String>>> {
    Json(liturgia::get_for_day(get_today(), None))
}
#[get("/<kind>")]
fn today_kind(kind: String) -> Json<Box<dyn erased_serde::Serialize>> {
    let kind: eprex_parser::types::DayPart = serde_json::from_str(&format!("\"{kind}\"")).unwrap();
    Json(kind.parse(liturgia::get_for_day(get_today(), Some(&kind))))
}
#[get("/<ts>", rank = 2)]
fn day_all(ts: u64) -> Json<Vec<Vec<String>>> {
    Json(liturgia::get_for_day(fix_date(ts), None))
}
#[get("/<ts>/<kind>")]
fn day_kind(ts: u64, kind: String) -> Json<Box<dyn erased_serde::Serialize>> {
    let kind: eprex_parser::types::DayPart = serde_json::from_str(&format!("\"{kind}\"")).unwrap();
    Json(kind.parse(liturgia::get_for_day(fix_date(ts), Some(&kind))))
}

#[launch]
fn rocket() -> _ {
    std::thread::spawn(|| {
        liturgia::get_for_day(get_today(), None);
        println!("Application ready");
    });
    rocket::build()
        .mount("/", routes![today_all, today_kind, day_all, day_kind])
        .attach({
            let cors = rocket_cors::CorsOptions {
                ..Default::default()
            }
            .to_cors()
            .unwrap();
            cors
        })
}
