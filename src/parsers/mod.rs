use erased_serde::Serialize;

pub mod liturgia;
pub trait Parse {
    fn parse(&self, data: Vec<Vec<String>>) -> Box<dyn Serialize>;
}

impl Parse for eprex_parser::types::DayPart {
    fn parse(&self, data: Vec<Vec<String>>) -> Box<dyn Serialize> {
        Box::new(match self {
            eprex_parser::types::DayPart::Ufficio => liturgia::parse(data),
            eprex_parser::types::DayPart::Lodi => liturgia::parse(data),
            eprex_parser::types::DayPart::Terza => liturgia::parse(data),
            eprex_parser::types::DayPart::Sesta => liturgia::parse(data),
            eprex_parser::types::DayPart::Nona => liturgia::parse(data),
            eprex_parser::types::DayPart::Vespri => liturgia::parse(data),
            eprex_parser::types::DayPart::Compieta => liturgia::parse(data),
            eprex_parser::types::DayPart::Liturgia => liturgia::parse(data),
            eprex_parser::types::DayPart::Santi => liturgia::parse(data),
            eprex_parser::types::DayPart::Defunti => liturgia::parse(data),
        })
    }
}
