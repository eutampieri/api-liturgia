use serde::Serialize;

#[derive(Serialize)]
pub struct Liturgia {
    pub nonsocomesichiami: String,
    pub colore_liturgico: String,
    pub prima_lettura: (String, String),
    pub salmo_responsoriale: (String, String, Vec<String>),
    pub seconda_lettura: Option<(String, String)>,
    pub canto_al_vangelo: String,
    pub vangelo: (String, String),
}

fn fix_lettura(pieces: &[String]) -> (String, String) {
    let re = regex::Regex::new(r"^\d?[A-Z][a-z]{1,2}(\s\d+(,\d*(-(\d*\w?\.?)+)*)?)?$").unwrap();
    let reference_position = pieces.iter().position(|x| re.captures(x).is_some());
    match reference_position {
        Some(pos) => {
            let reference = re
                .captures(&pieces[pos])
                .unwrap()
                .get(0)
                .unwrap()
                .as_str()
                .to_owned();
            (
                reference,
                pieces
                    .iter()
                    .skip(pos + 1)
                    .cloned()
                    .collect::<Vec<_>>()
                    .as_slice()
                    .join("\n")
                    .replace('\r', ""),
            )
        }
        None => (
            pieces[2].replace('\r', ""),
            pieces[3..].join("\n").replace('\r', ""),
        ),
    }
}

fn fix_salmo(pieces: &[String]) -> (String, String, Vec<String>) {
    let re = regex::Regex::new(r"Sal(mo)?\s\d{1,2}(\s\(\d{1,2}\))?").unwrap();
    let reference_position = pieces.iter().position(|x| re.captures(x).is_some());
    match reference_position {
        Some(pos) => {
            let reference = re
                .captures(&pieces[pos])
                .unwrap()
                .get(0)
                .unwrap()
                .as_str()
                .replace('\r', "");
            (
                reference,
                pieces[pos + 1].replace('\r', ""),
                pieces
                    .iter()
                    .skip(pos + 2)
                    .cloned()
                    .collect::<Vec<_>>()
                    .as_slice()
                    .join("\n")
                    .split('\r')
                    .map(|x| x.trim().to_owned())
                    .collect(),
            )
        }
        None => unimplemented!(),
    }
}

pub fn parse(v: Vec<Vec<String>>) -> Liturgia {
    let has_seconda_lettura = v.len() > 7;
    Liturgia {
        colore_liturgico: {
            let re = regex::Regex::new(r"(\*\*(\w*)\*\*)").unwrap();
            let caps = re.captures(&v[2][0]).unwrap();
            caps.get(2).unwrap().as_str().to_owned()
        },
        prima_lettura: fix_lettura(&v[3]),
        salmo_responsoriale: fix_salmo(&v[4]),
        seconda_lettura: {
            if has_seconda_lettura {
                Some(fix_lettura(&v[5]))
            } else {
                None
            }
        },
        canto_al_vangelo: v[5 + if has_seconda_lettura { 1 } else { 0 }]
            .iter()
            .filter(|x| x.find("Alleluia").is_none())
            .filter(|x| x.find("#").is_none())
            .filter(|x| {
                let re =
                    regex::Regex::new(r"^\(?\d?[A-Z][a-z]{1,2}(\s\d+(,\d*(-(\d*\w?\.?)+)*)?)?\)?$")
                        .unwrap();
                re.captures(x).is_none()
            })
            .map(|x| x.replace('\r', ""))
            .collect::<Vec<_>>()
            .as_slice()
            .join("\n"),
        vangelo: fix_lettura(&v[6 + if has_seconda_lettura { 1 } else { 0 }]),
        nonsocomesichiami: v[0]
            .iter()
            .map(|x| {
                let p: &[_] = &['#', ' '];
                x.trim_matches(p)
            })
            .collect::<Vec<_>>()
            .join(" - ")
            .replace('\r', ""),
    }
}
